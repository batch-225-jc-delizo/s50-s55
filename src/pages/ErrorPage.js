import React from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';

const NotFound = () => (
  <Container className="my-5">
    <Row className="justify-content-center">
      <Col md={8}>
        <Card className="text-center">
          <Card.Header>
            <h1 className="display-4">404 - Not Found</h1>
          </Card.Header>
          <Card.Body>
            <p className="lead">The page you are looking for does not exist.</p>
            <Button variant="primary" href="/">
              Go Home
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  </Container>
);

export default NotFound;
